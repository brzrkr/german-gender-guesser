# German Gender Guesser
A machine learning model to predict the grammatical gender
of German nouns.

This is a sister project to German Plural Guesser.

A pair of `gegegue` files containing the model configuration
and pre-trained weights is provided to ease out-of-the-box
prediction. The pre-trained model has approximately 89%
accuracy.

## Data preparation
The model is built to read from .csv files of the format:
`Noun,Masculine,Feminine,Neuter`
for each row, where the first is one German word and the
following three are 0/1 values of whether the noun accepts
the given gender. This supports German words which hold
more than one gender each. Plural gender is not included as
it is invariant in the German language.

The program expects a header row as the first row of the file,
with exactly the same content as the format specified above.

### extract-nouns.py
The `extract-nouns.py` script was built to extract the
aforementioned .csv file from
[dict.cc](https://www.dict.cc)'s corpus. It can also work on
different source files as long as it is formatted similarly.
Check the module docstring of the script for details.

The aforementioned corpus is not included here for license
reasons and must be obtained separately on their website.

## Usage
The `gegegue/main.py` module exposes the `Guesser` class.

Running the module directly will train the model on the
`nouns.csv` file and provide basic evaluation metrics.

To use the model, you can simply import `gegegue.main.Guesser`
and instantiate it. Instantiating `Guesser` with a file name
will automatically preprocess the data therein and train the
model. To predict from new data after this, see `predict()`
below.

It is also possible to save/load the model (see section below).

The following are the individual steps to take to use the model
from scratch, in their given order:

### preprocess()
`Guesser.preprocess()` takes one argument: `filename`, indicating
the file name of the data to use for training and testing.

This step must be taken first.

### compile()
`Guesser.compile()` takes no arguments and serves to set the
module's structure of neuron layers.

This must occur after preprocessing and before training.

### fit()
`Guesser.fit()` takes, as optional keyword arguments, all the
arguments `keras.model.fit()` would take, but default values are
provided and the method can thus be called with no arguments.

This method will actually train the model.

This must occur after compilation but before any further steps.

### evaluate()
`Guesser.evaluate()` takes all arguments `keras.model.evaluate()`
would take, but is also provided with defaults to run the method
with no arguments.

This method is optional and will provide an evaluation of the
performance of the model.

### predict()
After training the model, running `Guesser.predict()` with a list
of words will return a list of predictions, each itself being a
list of three elements: probability of masculine, feminine, and
neuter, in that order.

## Saving and loading
The model can be loaded by instantiating `Guesser` with a
`load` argument pointing to two files with the same name, with
extensions `.json` and `.h5` respectively for the model
configuration and weights.

To save the model, provide the `save` argument to `Guesser`,
which will produce two files analogous to the ones described
above.

Specifying both `save` and `load` effectively copies the model
over with no changes.

### Resuming learning
The model can be retrained on top of preexisting learning by
saving and then loading the entire model. To do this, provide
the `model_save` and `model_load` arguments with a file name
to use for saving and loading respectively.
After loading the model, additional training can occur either
by calling the `Guesser.fit()` method or automatically, if the
constructor has also been provided with a file name for the data.
