"""Utility script to extract german nouns and genders from a corpus.
    Copyright (c) 2018 Federico Salerno <itashadd+gegegue@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    NOTE: `noun.csv` is derived from the dict.cc corpus and is provided only
    for completeness and demonstration purposes. Its usage is subjected to
    the restrictions found at the following address:
    <https://www1.dict.cc/translation_file_request.php?l=e>

The script produces a new `nouns.csv` file with noun,m,f,n entries
where m, f, n are masculine, feminine and neuter and are either
1 or 0 depending on which gender(s) the word accepts according to
the corpus. The entries are in no particular order.

Modelled on dict.cc's choice of format and encoding, the corpus is
expected to be in the same directory, have the name `corpus.txt`,
be in UTF-8 encoding, and contain one lemma per line, with the word
`noun` at the end of each noun's line (may be followed by something
enclosed in [], which will be ignored), and the gender(s) in the
form of {m}, {f}, {n} for masculine, feminine, neuter respectively.
More than one gender is allowed for each noun. The noun itself is
considered to be the first capitalised word of the line (since
German nouns are always capitalised), ignoring any part between ()
and up to the gender letter that is enclosed in {}. The noun is
then un-capitalised and duplicates are ignored, but duplicates with
different genders are merged together.

Example:
    (akuter) Hörsturz {m}	acute hearing loss	noun	[med.]
    abnehmender Mond {m}	waning moon	noun
    Mond {m}	moon	noun

    becomes:
    hörsturz,1,0,0
    mond,1,0,0
"""

import regex as re
from collections import defaultdict
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Compile regex patterns.
noun_line = re.compile(r".*noun(\s)*(\[.*]\s*)*$")
noun_lemma = re.compile(r"([[:upper:]]\w+)")
noun_gender = re.compile(r"\{([mfn])\}")

if __name__ == '__main__':
    # Read the corpus.
    noun_genders = defaultdict(set)

    logger.info("Reading corpus...")

    with open("corpus.txt", "r", errors="surrogateescape", encoding="utf-8") as corpus:
        for line in corpus:
            logger.debug(f"Parsing line: {line}")

            # Skip all lines that don't have a "noun" in last position,
            # although [] are allowed as last element.
            if not re.match(noun_line, line):
                logger.debug(f"Skipped: does not have 'noun'.")
                continue

            # Extract lemma.
            lemma = re.search(noun_lemma, line)

            # If the lemma is not recognisable, skip it.
            if lemma is None:
                logger.debug(f"Skipped: lemma is not recognisable.")
                continue

            lemma = lemma[0]

            # Extract genders as a set of gender letters.
            gender_letters = {item.lower() for item in re.findall(noun_gender, line)}

            # Add the found gender letters to the lemma's.
            noun_genders[lemma] |= gender_letters

            logger.info(f"Lemma '{lemma}' has genders {noun_genders[lemma]}.")

    logger.info("Finished reading corpus.")

    # Write the output.
    with open("nouns.csv", "w+", newline="\r\n", errors="surrogateescape", encoding="utf-8") as output:
        logger.info("Writing output...")

        # Header of nouns with engineered features and labels.
        output.write("Noun,Masculine,Feminine,Neuter\n")
        output.writelines([f"{noun},"
                           # Labels:
                           f"{1 if 'm' in genders else 0},"
                           f"{1 if 'f' in genders else 0},"
                           f"{1 if 'n' in genders else 0}\n"
                           for noun, genders in noun_genders.items()
                           if genders.issubset({"m", "f", "n"})
                           and len(genders) > 0])

        logger.info("Finished writing output.")
