"""Gegegue (German Gender Guesser) is a model to predict the gender of German nouns.
Naming things is hard.
    Copyright (c) 2018 Federico Salerno <itashadd+gegegue@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    NOTE: `noun.csv` is derived from the dict.cc corpus and is provided only
    for completeness and demonstration purposes. Its usage is subjected to
    the restrictions found at the following address:
    <https://www1.dict.cc/translation_file_request.php?l=e>
"""

import json
from typing import Tuple, List, Sequence
from keras.preprocessing.text import Tokenizer
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json, load_model
import pandas as pd
import numpy as np
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
logger.addHandler(logging.StreamHandler())


class Guesser:
    def __init__(
            self,
            filename: str=None,
            save: str=None,
            load: str=None,
            model_save: str=None,
            model_load: str=None):
        """Initialise the model.

        If `filename` is provided, preprocessing and learning are
        performed automatically.

        If a file name is specified with `save`, the model
        configuration will be saved to it in .json format,
        and the weights will use the same filename in .h5.

        If a file name is specified with `load`, the model
        will be loaded from it, expecting .json and .h5
        files to load configuration and weights from.

        Specifying both load and save effectively copies
        the model without any changes.

        Model save and model load work differently by using the
        whole model and allowing retraining on the preexisting
        learned model.
        """

        self.save_model_file: str = model_save
        """File name to save the model to for resumable learning."""

        self.save_file: str = save
        """File name to save the model to. If None, no saving will occur."""

        self.load_file: str = load
        """File name to load the model from. If None, no loading will occur."""

        self.is_ready = False
        """True if the model has been prepared with training data.
        
        The model must be prepared by calling its preprocess() method
        or by calling the constructor with the filename parameter before
        it can be compiled and used.
        """

        self.is_compiled = False
        """True if the model has been compiled.
        
        The model must be compiled by calling its compile() method
        after its data has been preprocessed.
        """

        self.is_fit = False
        """True if the model has been fit.
        
        The model must be fit to the training data before being able to
        be evaluated or before attempting predictions of new data.
        """

        self.x_train: np.ndarray = None
        """Training features."""

        self.x_test: np.ndarray = None
        """Test features."""

        self.y_train: np.ndarray = None
        """Training labels."""

        self.y_test: np.ndarray = None
        """Test labels."""

        self.longest: int = None
        """Length of the longest word in the training set.
        
        Used for padding to have character sequences of equal length.
        """

        self.tokenizer: Tokenizer = None
        """Tokenizer for turning text features into integer sequences."""

        # Initialise the model.
        if model_load:
            logger.info(f"Loading whole model from file: {model_load}.h5")
            self.model = load_model(model_load + ".h5")
            self.is_ready = True
            self.is_compiled = True
            self.is_fit = True
        else:
            self.model = Sequential()

        if filename is not None:
            logger.info(f"Filename entered: {filename}")

            self.preprocess(filename)
            if not model_load:
                self.compile()
            self.fit()

    def __str__(self):
        return f"<Guesser {id(self)}: " \
               f"{'' if self.is_ready else 'not '}ready, " \
               f"{'' if self.is_compiled else 'not '}compiled, " \
               f"{'' if self.is_fit else 'not '}fit.>"

    def _tokenize(self, text: np.array) -> list:
        """Tokenize a text according to a defined tokenizer."""
        if not self.tokenizer:
            raise ValueError("No tokenizer detected. "
                             "Run preprocess() first.")

        logger.debug(f"Tokenizing text: {text.shape}")

        output = self.tokenizer.texts_to_sequences(text.flatten())

        logger.debug(f"Result sequences: {len(output)} example: {output[0]}")

        return output

    def _pad_sequences(self, data: Sequence) -> np.ndarray:
        """Turn text tokens into sequences padded to the longest word in the data."""
        if not self.longest:
            raise ValueError("No length detected for longest feature word. "
                             "Run preprocess() first.")

        logger.debug(f"Padding data to longest: {type(data)}, {len(data)}")

        return sequence.pad_sequences(data, maxlen=self.longest)

    @staticmethod
    def _engineer_features(dataset: pd.DataFrame) -> Tuple[List[str], List[str]]:
        """Create new features from existing ones.

        :param dataset: the dataset to derive new features. Will be modified in place.
        :return: a tuple of two lists:
            a list of column names of new features not requiring tokenization,
            a list of column names of new features requiring tokenization.
        """

        # Noun length.
        dataset["Noun_length"] = dataset["Noun"].apply(len)

        return ["Noun_length"], []

    @staticmethod
    def _pretty_print(input_nouns: Sequence, predictions: Sequence):
        """Print a list of nouns and corresponding predictions as a table."""
        if len(input_nouns) != len(predictions):
            raise ValueError("input_nouns and predictions must have the same length.")

        # Pad to the length of the longest predicted noun plus 5.
        pad_w = max(map(len, input_nouns)) + 5
        # Pad labels to 12.
        float_length = 6
        pad_l = 12

        # 2 is subtracted for the length of the title string itself.
        pad_l_title = " " * (pad_l - 2)
        # Minus the length of the float. 2 is added for the zero and dot.
        pad_l_content = " " * (pad_l - (float_length + 2))

        title_string = f"Word{' ' * (pad_w - 4)}" \
                       f"%M{pad_l_title}" \
                       f"%F{pad_l_title}" \
                       f"%N"
        content_string = "\n".join(f"{word}"
                                   f"{' ' * (pad_w - len(word))}"
                                   f"{genders[0]:1.{float_length}f}"
                                   f"{pad_l_content}"
                                   f"{genders[1]:1.{float_length}f}"
                                   f"{pad_l_content}"
                                   f"{genders[2]:1.{float_length}f}"
                                   for word, genders in zip(input_nouns, predictions))
        output_string = "\n".join((title_string, content_string))

        print(output_string)

    def preprocess(self, filename: str):
        """Preprocess and featurise training data.

        :param filename: CSV file with Noun,Masculine,Feminine,Neuter columns
            where the three genders are boolean values for each noun.
        """

        logger.info(f"Start preprocessing of {filename}")

        nouns = pd.read_csv(filename,
                            delimiter=",",
                            encoding="utf-8",
                            memory_map=True,
                            )
        logger.debug(f"Whole dataset = len {len(nouns)}, shape {nouns.shape}")

        self.is_ready = False

        new_features, new_features_tokenize = self._engineer_features(nouns)

        labels = ["Masculine", "Feminine", "Neuter"]
        all_features = ["Noun"] + new_features + new_features_tokenize
        tokenize_features = ["Noun"] + new_features_tokenize

        # Mask to pick 80% of the dataset as training data.
        training = np.random.rand(len(nouns)) < 0.8
        logger.debug(f"Training mask 80% = len {len(training)}, shape {training.shape}")

        # Training and test features, excluding labels.
        X_train = nouns.loc[training, all_features]
        logger.debug(f"X_train = len {len(X_train)}, shape {X_train.shape}")
        X_test = nouns.loc[~training, all_features]
        logger.debug(f"X_test = len {len(X_test)}, shape {X_test.shape}")

        # Training and test labels with each gender.
        self.y_train = nouns.loc[training, labels]
        logger.debug(f"self.y_train = len {len(self.y_train)}, shape {self.y_train.shape}")
        self.y_test = nouns.loc[~training, labels]
        logger.debug(f"self.y_test = len {len(self.y_test)}, shape {self.y_test.shape}")

        # Tokenization of each noun's characters.
        self.tokenizer = Tokenizer(char_level=True)
        logger.debug(f"Fitting tokenizer on Noun = "
                     f"type {type(nouns['Noun'].values)}, "
                     f"len {len(nouns['Noun'].values)}, "
                     f"shape {nouns['Noun'].values.shape},"
                     f"content:\n{nouns['Noun'].values}")
        self.tokenizer.fit_on_texts(nouns["Noun"].values)

        self.x_train = self._tokenize(X_train[tokenize_features].values)
        logger.debug(f"Tokenized X_train to self.x_train = "
                     f"type {type(self.x_train)},"
                     f"len {len(self.x_train)}")
        self.x_test = self._tokenize(X_test[tokenize_features].values)
        logger.debug(f"Tokenized X_test to self.x_test = "
                     f"type {type(self.x_test)},"
                     f"len {len(self.x_test)}")

        # Pad sequences to the length of the longest word.
        self.longest = max(map(len, self.x_test + self.x_train))
        logger.debug(f"Longest: {self.longest}")

        self.x_train = self._pad_sequences(self.x_train)
        self.x_test = self._pad_sequences(self.x_test)

        # Add non-tokenizable features
        logger.debug(f"X_train new features to add: {X_train[new_features].values.shape}, "
                     f"{X_train[new_features].values[0]}")
        logger.debug(f"X_test new features to add: {X_test[new_features].values.shape}, "
                     f"{X_test[new_features].values[0]}")

        logger.debug(f"x_train before adding features: {self.x_train.shape}, {self.x_train[0]}")
        logger.debug(f"x_test before adding features: {self.x_test.shape}, {self.x_test[0]}")
        self.x_train = np.c_[self.x_train, X_train[new_features].values]
        self.x_test = np.c_[self.x_test, X_test[new_features].values]
        logger.debug(f"x_train after adding features: {self.x_train.shape}, {self.x_train[0]}")
        logger.debug(f"x_test after adding features: {self.x_test.shape}, {self.x_test[0]}")

        self.is_ready = True

        logger.info(f"Finished preprocessing.")

    def compile(self):
        """Compile the module before fitting."""
        if not self.is_ready:
            raise RuntimeError("Model data must be preprocessed before compilation.")

        self.is_compiled = False

        if self.load_file:
            logger.info(f"Loading model configuration from file {self.load_file}.json...")
            # Load with json module to circumvent decoding error.
            with open(self.load_file + ".json") as file:
                json_model = json.load(file)
                self.model = model_from_json(json.dumps(json_model))
            logger.info("Finished loading.")
        else:
            # First parameterised PReLU layer to only learn meaningful weights and
            # learn the meaningfulness of the weights as we go.
            # Input shape must be specified to circumvent saving error internal to keras.
            self.model.add(Dense(self.longest, activation="relu", input_shape=(self.x_train.shape[1],)))

            self.model.add(Dense(self.longest, activation="relu"))
            self.model.add(Dense(self.longest, activation="relu"))
            self.model.add(Dense(self.longest, activation="relu"))
            self.model.add(Dense(self.longest, activation="relu"))

            # Output layer with one neuron per gender (m, f, n) and tan+sigmoid
            # activation for independent probability of each class's correctness.
            self.model.add(Dense(3, activation="tanh"))
            self.model.add(Dense(3, activation="sigmoid"))

        logger.info(f"Compiling model.")

        self.model.compile(
            # Binary crossentropy for loss of each class independent from others.
            loss="binary_crossentropy",
            optimizer="adam",
            metrics=["accuracy"],
        )

        self.is_compiled = True

        logger.info(f"Finished compiling.")

        if self.save_file:
            logger.info(f"Saving model configuration to file {self.save_file}.json.")
            with open(self.save_file + ".json", "w") as file:
                file.write(self.model.to_json())
            logger.info("Finished saving.")

    def fit(self, **kwargs):
        """Fit the model with the given arguments.

        By default, the model uses the data from its preprocess status.
        """

        if not self.is_ready:
            raise RuntimeError("Model data must be preprocessed before fitting.")

        if not self.is_compiled:
            raise RuntimeError("Model must be compiled before fitting.")

        self.is_fit = False

        if self.load_file:
            logger.info(f"Loading model weights from file {self.load_file}.h5.")
            self.model.load_weights(self.load_file + ".h5")
            logger.info("Finished loading.")
        else:
            logger.info(f"Fitting model.")
            logger.debug(f"With kwargs:\n{kwargs}")

            # Defaults:
            kwargs.setdefault("x", np.array(self.x_train))
            kwargs.setdefault("y", np.array(self.y_train))
            kwargs.setdefault("batch_size", 32)
            kwargs.setdefault("epochs", 10)
            kwargs.setdefault("validation_split", 0.1)
            logger.debug(f"Defaults set. kwargs:\n{kwargs}")

            logger.debug(f"X: {kwargs['x'].shape}, {len(kwargs['x'])}")
            logger.debug(f"Y: {kwargs['y'].shape}, {len(kwargs['y'])}")
            self.model.fit(**kwargs)

        self.is_fit = True

        logger.info(f"Finished fitting.")

        if self.save_model_file:
            logger.info(f"Saving whole model {self.save_model_file}.h5.")
            self.model.save(self.save_model_file + ".h5")
            logger.info("Finished saving.")

        if self.save_file:
            logger.info(f"Saving model weights to file {self.save_file}.json.")
            self.model.save_weights(self.save_file + ".h5")
            logger.info("Finished saving.")

    def evaluate(self, **kwargs):
        """Evaluate the model's performance.

        By default, the test data and labels defined in preprocessing are
        used and the batch size is equal to 64.
        """

        if not self.is_ready:
            raise RuntimeError("Model data must be preprocessed before evaluation.")

        if not self.is_compiled:
            raise RuntimeError("Model must be compiled before evaluation.")

        if not self.is_fit:
            raise RuntimeError("Model must be fit before evaluation.")

        logger.info(f"Evaluating model.")
        logger.debug(f"With kwargs:\n{kwargs}")

        # Defaults:
        kwargs.setdefault("x", self.x_test)
        kwargs.setdefault("y", self.y_test)
        kwargs.setdefault("batch_size", 32)
        logger.debug(f"Defaults set. kwargs:\n{kwargs}")

        logger.debug(f"X: {kwargs['x'].shape}, {len(kwargs['x'])}")
        logger.debug(f"Y: {kwargs['y'].shape}, {len(kwargs['y'])}")

        evaluation = self.model.evaluate(**kwargs)
        print("\n".join(f"{label}: {metric}" for label, metric in zip(self.model.metrics_names, evaluation)))

    def predict(self, input_nouns: Sequence):
        """Predict the gender of the given nouns."""
        if not self.is_ready:
            raise RuntimeError("Model data must be preprocessed before prediction.")

        if not self.is_compiled:
            raise RuntimeError("Model must be compiled before prediction.")

        if not self.is_fit:
            raise RuntimeError("Model must be fit before prediction.")

        logger.info(f"Predicting output for input:\n{input}")

        x = np.array(input_nouns)

        x = self._tokenize(x)
        x = self._pad_sequences(x)

        # Add extra features
        extra = np.ndarray(shape=(len(input_nouns), 1))
        for i, length in enumerate(map(len, input_nouns)):
            extra[i] = length

        x = np.c_[x, extra]

        predictions = self.model.predict(x=x, batch_size=1, verbose=1)
        logger.debug(f"Predictions: {type(predictions)} {predictions.shape} {predictions.size}")

        # Pretty print results.
        self._pretty_print(input_nouns, predictions)


if __name__ == '__main__':
    logger.setLevel(logging.DEBUG)

    guesser = Guesser(filename="nouns.csv", load="gegegue")
    guesser.evaluate()
    guesser.predict([])  # Some test words
